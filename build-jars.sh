#!/bin/sh

# Create tequila-idp.jar
mkdir -p dist
cd tequila-idp
rm -r ../dist/*
zip -r ../dist/tequila-idp.jar *
unzip -l ../dist/tequila-idp.jar