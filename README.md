# AGEPoly's Keycloak Providers

We had to extend some basic functionnalities of Keycloak for our needs. This repo includes the code to build the following Keycloak providers :
- Tequila Identity Provider (tequila-idp.jar)

Generate the jars with `./build-jars.sh` **then commit** to Gitlab. Gitlab artifacts are shit so we don't use CI.  

## How to use the JAR files

Keycloak has a "providers" folder which can load the JARs at startup.

With [the image packaged by Bitnami](https://hub.docker.com/r/bitnami/keycloak/) the corresponding helm chart can use the following values:
```yaml
initdbScripts:
    load_custom_provider_script.sh: |
      #!/bin/bash
      echo "Downloading custom providers"
      wget https://gitlab.com/agepoly/it/dev/keycloak-providers/-/jobs/4635730397/artifacts/file/dist/tequila-idp.jar -O /opt/bitnami/keycloak/providers/tequila-idp.jar
```

## Useful links

- [org.keycloak.authentication.authenticators.browser.ScriptBasedAuthenticatorFactory](https://www.keycloak.org/docs-api/22.0.1/javadocs/org/keycloak/authentication/authenticators/browser/ScriptBasedAuthenticatorFactory.html)
- adapted from https://gitlab.com/guenoledc-perso/keycloak/rest-authenticator
- [Nashorn Wiki](https://wiki.openjdk.java.net/display/Nashorn/) (engine for JS used by Java)